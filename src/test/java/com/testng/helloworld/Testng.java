package com.testng.helloworld;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterClass;

public class Testng {
  @Test
  public void f() {
	  System.out.println("check1");
  }
  @BeforeMethod
  public void beforeMethod() {
	  System.out.println("check2");
  }

  @AfterMethod
  public void afterMethod() {
	  System.out.println("check3");
  }

  @AfterClass
  public void afterClass() {
	  System.out.println("check4");
  }

}
